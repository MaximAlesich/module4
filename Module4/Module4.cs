using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            throw new NotSupportedException();
        }

        public int Task_1_A(int[] array)
        {
            if (array == null || array.Length <= 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int maxElement = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (maxElement < array[i])
                {
                    maxElement = array[i];
                }
            }
            return maxElement;
        }

        public int Task_1_B(int[] array)
        {
            if (array == null || array.Length <= 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int minElement = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (minElement > array[i])
                {
                    minElement = array[i];
                }
            }
            return minElement;
        }

        public int Task_1_C(int[] array)
        {
            if (array == null || array.Length <= 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int sumElement = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sumElement += array[i];
            }
            return sumElement;
        }

        public int Task_1_D(int[] array)
        {
            if (array == null || array.Length <= 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int differenceNum = 0;
            int maxNum = array.Max(a => a);
            int minNum = array.Min(b => b);
            foreach (int i in array)
            {
                differenceNum = maxNum - minNum;
            }
            return differenceNum;
        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length <= 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int maxNum = array.Max();
            int minNum = array.Min();

            for (int i = 0; i < array.Length; i++)
            {
                if (i % 2 == 0)
                {
                    array[i] += maxNum;
                }
                else if (i % 2 != 0)
                {
                    array[i] -= minNum;
                }
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            double d = a + b + c;
            return d;
        }

        public string Task_2(string a, string b)
        {
            string c = $"{a}{b}";
            return c;

        }

        public int[] Task_2(int[] a, int[] b)
        {
            int[] sumArray = new int[Math.Max(a.Length, b.Length)];

            for (int i = 0; i < sumArray.Length; i++)
            {
                if (i < a.Length)
                {
                    sumArray[i] += a[i];
                }
                if (i < b.Length)
                {
                    sumArray[i] += b[i];
                }
            }
            return sumArray;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
            {
                throw new ArgumentNullException(nameof(radius));
            }

            length = 2 * Math.PI * radius;
            square = Math.PI * radius * radius;
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            if (array == null || array.Length <= 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            maxItem = array.Max<int>();
            minItem = array.Min<int>();

            sumOfItems = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sumOfItems += array[i];
            }
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            (int, int, int) outRresult = (numbers);
            int a = outRresult.Item1 += 10;
            int b = outRresult.Item2 += 10;
            int c = outRresult.Item3 += 10;
            return (a, b, c);
        }

        public (double, double) Task_4_B(double radius)
        {

            if (radius < 0)
            {
                throw new ArgumentNullException(nameof(radius));
            }

            double length = 2 * Math.PI * radius;
            double square = Math.PI * radius * radius;
            return (length, square);
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            if (array == null || array.Length <= 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int max = array[0];
            int min = array[0];
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < max)
                {
                    max = array[i];
                }
                else if (array[i] > min)
                {
                    min = array[i];
                }

                sum += array[i];

            }
            return (max, min, sum);
        }

        public void Task_5(int[] array)
        {
            if (array == null || array.Length <= 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int[] resultArray = new int[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                resultArray[i] = array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length <= 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (int i = 0; i < array.Length; i++)
            {
                if (direction == SortDirection.Ascending)
                {
                    Array.Sort(array);
                }
                else if (direction == SortDirection.Descending)
                {
                    Array.Sort(array);
                    Array.Reverse(array);
                }
            }
        }

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {

           double cutInHalf = (x1 + x2) / 2;

            if ((func(x1) * func(cutInHalf)) < 0)
            {
                cutInHalf = x2;
            }
            else if ((func(x2) * func(cutInHalf)) < 0)
            {
                cutInHalf = x1;
            }

            cutInHalf = (x1 + x2) / 2;

            return Math.Round(cutInHalf, 2);
        }
    }
}